package com.cto.cloud.entity;

/**
 * @author Zhang Yongwei
 * @version 1.0
 * @date 2019-12-23
 */
public class CustomConstants {
    /**
     * 分页参数
     */
    public static class PageQuery{
        public static final int DEFAULT_PAGE_NUMBER = 1;
        public static final int DEFAULT_PAGE_SIZE = 15;
        public static final String DEFAULT_SORT = "id desc";
    }

    /**
     * Reids参数
     */
    public static class Redis{
        public static final String FRONT_USER_TOKEN = "front_user_token:";
        public static final String ADMIN_USER_TOKEN = "admin_user_token:";
    }

    /**
     * 系统参数
     */
    public static class System{
        public static final String FRONT_URL = "front";
        public static final String MANAGER_URL = "/manager";
        public static final String FRONT_USER_SESSION = "front_user_session";
        public static final String MANAGER_USER_SESSION = "manager_user_session";
    }

    /**
     * rabbitMq队列参数
     */
    public static class RabbitMq{
        /**邮件**/
        public static final String MQ_MAIL = "邮件消息";
        public static final String CTO_MAIL_QUEUE = "cto.mail.queue";
        public static final String CTO_MAIL_EXCHANGE = "cto.mail.exchange";
        public static final String CTO_MAIL_ROUTING_KEY = "cto.mail.routing.key";
        /**日志**/
        public static final String MQ_LOG = "日志消息";
        public static final String CTO_LOG_QUEUE = "cto.log.queue";
        public static final String CTO_LOG_EXCHANGE = "cto.log.exchange";
        public static final String CTO_LOG_ROUTING_KEY = "cto.log.routing.key";
    }
}
