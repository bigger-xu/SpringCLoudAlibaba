package com.cto.cloud.entity;

/**
 * @author Zhang Yongwei
 * @version 1.0
 * @date 2019-12-16
 */
public enum ResponseCode {
    /**
     * 系统参数
     */
    SUCCESS(0,"请求成功"),
    FAIL(-1,"请求失败"),
    SERVICE_TIME_OUT(-2,"服务请求超时,请稍后重试"),

    /**
     * 前台用户
     */
    USER_NAME_IS_NOT_NULL(1001,"用户名不能为空"),
    EMAIL_IS_NOT_NULL(1002,"邮箱不能为空"),
    USER_NAME_IS_REPEAT(1003,"用户名重复"),
    EMAIL_IS_REPEAT(1004,"邮箱重复"),
    GET_TOKEN_ERROR(1005,"获取token失败"),
    USER_IS_NULL(1006,"用户不存在或密码错误"),
    CHECK_TOKEN_FAIL(1007,"token验证失败,已过期,请重新登录");

    private Integer code;
    private String message;
    ResponseCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
