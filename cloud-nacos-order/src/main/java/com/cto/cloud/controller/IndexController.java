package com.cto.cloud.controller;


import com.cto.cloud.annotation.IgnoreResponseAdvice;
import com.cto.cloud.service.PatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Bigger-Xu
 * @date 2020/6/22 16:35
 * @version v1.0.1
 */
@RestController
@RequestMapping("/order")
public class IndexController {

    private final PatService patService;
    @Autowired
    public IndexController(PatService patService) {
        this.patService = patService;
    }

    @GetMapping("get")
    @IgnoreResponseAdvice
    public Object get(){
        return patService.get();
    }

    @GetMapping("get1/{id}")
    @IgnoreResponseAdvice
    public Object get1(@PathVariable("id") String id){
        return patService.get1(id);
    }
}
