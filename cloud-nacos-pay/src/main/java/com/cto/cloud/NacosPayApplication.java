package com.cto.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 
 * @author Zhang Wei
 * @date 2020/5/17 01:29
 * @version v1.0.1
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NacosPayApplication {
    public static void main(String[] args) {
        SpringApplication.run(NacosPayApplication.class, args);
    }
}

