package com.cto.cloud.controller;

import com.cto.cloud.entity.CustomConstants;
import com.cto.cloud.exception.CustomException;
import com.cto.cloud.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Zhang Wei
 * @date 2020/5/17 01:55
 * @version v1.0.1
 */
@RestController
@RequestMapping("/pay")
public class IndexController {

    @Value("${server.port}")
    private String port;

    @GetMapping("get")
    public Object get(){
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return port + "-----" + UUIDUtils.getRandom(16);
    }
    @GetMapping("get1/{id}")
    public Object get1(@PathVariable("id") String id) throws CustomException {
        if(Integer.valueOf(id) < 0){
            throw new CustomException("主动异常");
        }
        return port + "-----" + id;
    }
}
