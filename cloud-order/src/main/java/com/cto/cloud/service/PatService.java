package com.cto.cloud.service;

import com.cto.cloud.entity.CommonResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Zhang Wei
 * @version v1.0.1
 * @date 2020/5/18 20:42
 */
@Component
@FeignClient(value = "CLOUD-PAY",fallback = PatServiceHystrix.class)
public interface PatService {

    @GetMapping("/pay/get")
    CommonResponse<Object> get();

    @GetMapping("/pay/get1/{id}")
    CommonResponse<Object> get1(@PathVariable("id") String id);
}
