package com.cto.cloud.service;

import com.cto.cloud.entity.CommonResponse;
import com.cto.cloud.entity.ResponseCode;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 
 * @author Zhang Wei
 * @date 2020/5/18 23:25
 * @version v1.0.1
 */
@Component
public class PatServiceHystrix implements PatService{
    @Override
    public CommonResponse<Object> get() {
        return new CommonResponse<>(ResponseCode.FAIL.getCode(),ResponseCode.FAIL.getMessage());
    }

    @Override
    public CommonResponse<Object> get1(@PathVariable String id) {
        return new CommonResponse<>(ResponseCode.FAIL.getCode(),ResponseCode.FAIL.getMessage());
    }
}
