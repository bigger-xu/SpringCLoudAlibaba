package com.cto.cloud.controller;

import com.cto.cloud.entity.CommonResponse;
import com.cto.cloud.entity.CustomConstants;
import com.cto.cloud.entity.ResponseCode;
import com.cto.cloud.exception.CustomException;
import com.cto.cloud.redis.RedisCacheService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Zhang Wei
 * @date 2020/5/17 01:55
 * @version v1.0.1
 */
@RestController
@RequestMapping("/pay")
public class IndexController {

    private final RedisCacheService redisCacheService;

    @Value("${server.port}")
    private String port;

    @Autowired
    public IndexController(RedisCacheService redisCacheService) {
        this.redisCacheService = redisCacheService;
    }

    @PostMapping("/insert")
    public CommonResponse insert(String json){
        redisCacheService.setCacheString(CustomConstants.Redis.FRONT_USER_TOKEN,json);
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(),ResponseCode.SUCCESS.getMessage());
    }
    @GetMapping("get")
    @HystrixCommand(fallbackMethod = "getError",commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "3000")
    })
    public Object get(){
        String key = redisCacheService.getCacheString(CustomConstants.Redis.FRONT_USER_TOKEN);
        return port + "-----" + key;
    }
    @GetMapping("get1/{id}")
    @HystrixCommand(fallbackMethod = "getError",commandProperties = {
            @HystrixProperty(name = "circuitBreaker.enabled",value = "true"),
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "10"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "10000"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "50")
    })
    public Object get1(@PathVariable("id") String id) throws CustomException {
        if(Integer.valueOf(id) < 0){
            throw new CustomException("主动异常");
        }
        String key = redisCacheService.getCacheString(CustomConstants.Redis.FRONT_USER_TOKEN);
        return port + "-----" + key;
    }

    public Object getError(){
        return "报错啦";
    }
    public Object getError(@PathVariable String id){
        return "报错啦";
    }
}
