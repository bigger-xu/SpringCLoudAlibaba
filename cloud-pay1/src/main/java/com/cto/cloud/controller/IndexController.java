package com.cto.cloud.controller;

import com.cto.cloud.entity.CommonResponse;
import com.cto.cloud.entity.CustomConstants;
import com.cto.cloud.entity.ResponseCode;
import com.cto.cloud.redis.RedisCacheService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Zhang Wei
 * @date 2020/5/17 01:55
 * @version v1.0.1
 */
@RestController
@RequestMapping("/pay")
public class IndexController {

    private final RedisCacheService redisCacheService;

    @Value("${server.port}")
    private String port;

    @Autowired
    public IndexController(RedisCacheService redisCacheService) {
        this.redisCacheService = redisCacheService;
    }

    @PostMapping("/insert")
    public CommonResponse insert(String json){
        redisCacheService.setCacheString(CustomConstants.Redis.FRONT_USER_TOKEN,json);
        return new CommonResponse<>(ResponseCode.SUCCESS.getCode(),ResponseCode.SUCCESS.getMessage());
    }
    @GetMapping("get")
    @HystrixCommand(fallbackMethod = "getError",commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "6000")
    })
    public Object get(){
        String key = redisCacheService.getCacheString(CustomConstants.Redis.FRONT_USER_TOKEN);
        return port + "-----" + key;
    }

    public Object getError(){
        return "报错啦";
    }
}
